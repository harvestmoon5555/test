<?php

use App\Http\Api\V1\ProductApi;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Api" middleware group. Enjoy building your API!
|
*/
//V1
Route::prefix('v1')->group(function () {

    //Product
    Route::prefix('product')->group(function () {
        Route::get('/{id}', [ProductApi::class, 'getOneProduct'])->name('getOneProduct');
        Route::get('/buy/{id}', [ProductApi::class, 'buyProduct'])->name('buyOneProduct');
        Route::put('/', [ProductApi::class, 'createProduct'])->name('createOneProduct');
        Route::patch('/{id}', [ProductApi::class, 'updateProduct'])->name('updateOneProduct');
        Route::delete('/{id}', [ProductApi::class, 'deleteProduct'])->name('deleteOneProduct');
    });
});
