<?php

namespace App\Http\Api\V1;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Services\ProductService;
use App\Repositories\contracts\ProductServiceInterface;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ProductApi
{
    public function __construct(protected ProductServiceInterface $productService)
    {
    }

    public function getOneProduct(int $id): JsonResponse
    {
        $product = $this->productService->getOneProduct($id);
        return new JsonResponse(
            [
                'message' => 'Product was updated successfully!',
                'status' => 'success',
                'product' => $product
            ],
            Response::HTTP_OK
        );
    }

    public function createProduct(CreateProductRequest $request): JsonResponse
    {
        $product = $this->productService->createProduct($request->validated());
        return new JsonResponse(
            [
                'message' => 'Product was created successfully!',
                'status' => 'success',
                'product' => $product
            ],
            Response::HTTP_CREATED
        );
    }

    public function updateProduct(UpdateProductRequest $request, int $id): JsonResponse
    {
        $result = $this->productService->updateProduct($request->validated(), $id);

        return new JsonResponse(
            [
                'message' => 'Product was updated successfully!',
                'status' => 'success',
                'product' => $result
            ],
            Response::HTTP_OK
        );
    }

    public function deleteProduct(int $id): JsonResponse
    {
         $this->productService->deleteProduct($id);
            return new JsonResponse(
                [
                    'message' => 'Product was deleted successfully!',
                    'status' => 'success'
                ],
                Response::HTTP_OK
            );
    }

    public function buyProduct(int $id): JsonResponse
    {
        $result=$this->productService->buyProduct($id);
        if ($result) {
            return new JsonResponse(
                [
                    'message' => 'Product shipment was successful!',
                    'status' => 'success'
                ],
                Response::HTTP_OK
            );
        } else {
            return new JsonResponse(
                [
                    'message' => 'Something happened!',
                    'status' => 'error'
                ],
                Response::HTTP_OK
            );
        }
    }

}
