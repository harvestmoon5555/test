<?php

namespace App\Http\Resources;

use App\Models\ProductMeta;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductMetaResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var ProductMeta $this */
        return [
            'id' => $this->id,
            'key' => $this->key,
            'value' => $this->value
        ];
    }
}
