<?php

namespace App\Http\Resources;

use App\Models\Content;
use Illuminate\Http\Resources\Json\JsonResource;

class ContentResource extends JsonResource
{
    public function toArray($request): array
    {
        /** @var Content $this */
        return [
            'id' => $this->id,
            'path' => $this->path,
            'hash' => $this->hash
        ];
    }
}
