<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * @param $request
     * @return array
     */
    public function toArray($request)
    {
        /** @var Product $this */
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'amount' => $this->amount,
            'count' => $this->count,
            'price' => $this->price,
            'type' => $this->type,
            'metas' => ProductMetaResource::Collection($this->metas),
            'contents' => ContentResource::Collection($this->contents)
        ];
    }
}
