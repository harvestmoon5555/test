<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'description' => 'required',
            'amount' => 'required|numeric',
            'count' => 'required|numeric',
            'price' => 'required|numeric',
            'type' => 'required',
            'img' => 'required',
            'img.*' => 'image',
            'metas' => 'required',
            'metas.*.key' => 'required',
            'metas.*.value' => 'required',
        ];
    }
}
