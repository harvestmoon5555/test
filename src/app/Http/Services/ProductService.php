<?php

namespace App\Http\Services;

use App\Exceptions\DeleteProductException;
use App\Exceptions\DeleteProductMetaException;
use App\Exceptions\ProductNotFoundException;
use App\Http\Resources\ProductResource;
use App\Jobs\BuyProduct;
use App\Models\Product;
use App\Repositories\contracts\ContentRepositoryInterface;
use App\Repositories\contracts\ProductMetaRepositoryInterface;
use App\Repositories\contracts\ProductRepositoryInterface;
use App\Repositories\contracts\ProductServiceInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductService implements ProductServiceInterface
{
    public function __construct(
        protected ProductRepositoryInterface $productRepository,
        protected ProductMetaRepositoryInterface $productMetaRepository,
        protected ContentRepositoryInterface $contentRepository
    ) {
    }

    /**
     * @throws ProductNotFoundException
     */
    public function getOneProduct(int $id): ProductResource
    {
        $product = DB::transaction(function () use ($id) {
            $product = $this->productRepository->getById($id);
            //If product does not exist
            $product ?? throw new ProductNotFoundException();
            //Bind product necessary data
            $product->metas = $this->productMetaRepository->getByProductId($id);
            $product->contents = $this->contentRepository->getByContentable($product->id, Product::class);
            return $product;
        });


        return new ProductResource($product);
    }

    public function createProduct(array $data): ProductResource
    {
        $product = DB::transaction(function () use ($data) {
            $product = $this->productRepository->create(
                [
                    'title' => $data['title'],
                    'description' => $data['description'],
                    'amount' => $data['amount'],
                    'count' => $data['count'],
                    'price' => $data['price'],
                    'type' => $data['type'],
                ]
            );

            //Make product content
            foreach ($data['img'] as $img) {
                $hash = Str::uuid()->getHex();
                $path = Storage::disk('public')->put($hash, $img);
                $this->contentRepository->create([
                    'hash' => $hash,
                    'path' => $path,
                    'contentable_id' => $product->id,
                    'contentable_type' => Product::class
                ]);
            }

            //Make Product Metas
            foreach ($data['metas'] as $meta) {
                $this->productMetaRepository->create([
                    'product_id' => $product->id,
                    'key' => $meta['key'],
                    'value' => $meta['value'],
                ]);
            }

            //Bind product necessary data
            $product->metas = $this->productMetaRepository->getByProductId($product->id);
            $product->contents = $this->contentRepository->getByContentable($product->id, Product::class);
            return $product;
        });

        return new ProductResource($product);
    }

    /**
     * @throws ProductNotFoundException
     */
    public function updateProduct(array $data, int $id): ProductResource
    {
        $product = DB::transaction(function () use ($data, $id) {
            $product = $this->productRepository->getById($id);
            //If product does not exist
            $product ?? throw new ProductNotFoundException();

            $this->productRepository->update([
                'title' => $data['title'],
                'description' => $data['description'],
                'amount' => $data['amount'],
                'count' => $data['count'],
                'price' => $data['price'],
                'type' => $data['type'],
            ], $id);
            $product = $this->productRepository->getById($id);

            //Update contents
            if (!empty($data['img'])) {
                //Delete previous contents
                $contents = $this->contentRepository->getByContentable($product->id, Product::class);
                foreach ($contents as $c) {
                    Storage::disk('public')->deleteDirectory($c->hash);
                    $this->contentRepository->delete($c->id);
                }

                foreach ($data['img'] as $img) {
                    $hash = Str::uuid()->getHex();
                    $path = Storage::disk('public')->put($hash, $img);
                    $this->contentRepository->create([
                        'hash' => $hash,
                        'path' => $path,
                        'contentable_id' => $product->id,
                        'contentable_type' => Product::class
                    ]);
                }
            }

            //Make Product Metas
            foreach ($data['metas'] as $meta) {
                $this->productMetaRepository->create([
                    'product_id' => $product->id,
                    'key' => $meta['key'],
                    'value' => $meta['value'],
                ]);
            }

            //Bind product necessary data
            $product->metas = $this->productMetaRepository->getByProductId($product->id);
            $product->contents = $this->contentRepository->getByContentable($product->id, Product::class);
            return $product;
        });
        return new ProductResource($product);
    }

    /**
     * @throws DeleteProductMetaException
     * @throws ProductNotFoundException
     * @throws DeleteProductException
     */
    public function deleteProduct(int $id): void
    {
        DB::transaction(function () use ($id) {
            $product = $this->productRepository->getById($id);

            //If product does not exit
            $product ?? throw new ProductNotFoundException();

            $productResult = $this->productRepository->delete($id);
            //If deleting product wasn't successful
            if (!$productResult) {
                throw new DeleteProductException();
            }

            //Delete contents files
            $contents = $this->contentRepository->getByContentable($id, Product::class);
            foreach ($contents as $c) {
                Storage::disk('public')->deleteDirectory($c->hash);
            }
            $contentResult = $this->contentRepository->deleteByContentable($id, Product::class);
            //If deleting product meta wasn't successful
            if (!$contentResult) {
                throw new DeleteProductMetaException();
            }
        });
    }

    /**
     * @throws ProductNotFoundException
     */
    public function buyProduct(int $id): bool
    {
        $product = $this->productRepository->getById($id);
        //If product does not exist
        $product ?? throw new ProductNotFoundException();
        BuyProduct::dispatch($product);
        return true;
    }
}