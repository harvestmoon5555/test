<?php
namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

abstract class ApplicationException extends Exception
{
    abstract public function statusCode(): int;

    abstract public function status(): string;

    abstract public function message(): string;

    public function render(Request $request): Response
    {
        $error = new Error($this->status(), $this->message());
        return response($error->toArray(), $this->statusCode());
    }
}