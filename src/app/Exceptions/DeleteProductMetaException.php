<?php

namespace App\Exceptions;

use Illuminate\Http\Response;

class DeleteProductMetaException extends ApplicationException
{
    public function statusCode(): int
    {
        return Response::HTTP_EXPECTATION_FAILED;
    }

    public function status(): string
    {
        return 'error';
    }

    public function message(): string
    {
        return 'Could not delete product meta!';
    }
}