<?php

namespace App\Exceptions;

use Illuminate\Http\Response;

class ProductNotFoundException extends ApplicationException
{

    public function statusCode(): int
    {
        return Response::HTTP_NOT_FOUND;
    }

    public function status(): string
    {
        return 'error';
    }

    public function message(): string
    {
       return 'This product does not exists!';
    }
}