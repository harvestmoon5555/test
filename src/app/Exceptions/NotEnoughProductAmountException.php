<?php

namespace App\Exceptions;

use Illuminate\Http\Response;

class NotEnoughProductAmountException extends ApplicationException
{

    public function statusCode(): int
    {
        return Response::HTTP_NOT_ACCEPTABLE;
    }

    public function status(): string
    {
        return 'error';
    }

    public function message(): string
    {
        return 'Not enough product!';
    }
}