<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * Class Content
 * @package App\Models
 *
 * @property int $id
 * @property string $path
 * @property string $hash
 */
class Content extends Model
{
    use HasFactory;

    protected $guarded=[];

}
