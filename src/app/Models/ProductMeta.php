<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductMeta
 * @package App\Models
 *
 * @property int $id
 * @property int $product_id
 * @property string $key
 * @property integer $value
 *
 * @property Product $product
 */
class ProductMeta extends Model
{
    use HasFactory;

    protected $guarded=[];

}
