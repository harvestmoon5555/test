<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property integer $amount
 * @property integer $count
 * @property float $price
 * @property string $type
 *
 * @property ProductMeta[] $metas
 * @property Content[] $contents
 */
class Product extends Model
{
    use HasFactory;

    protected $guarded=[];
}
