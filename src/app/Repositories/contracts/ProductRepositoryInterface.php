<?php

namespace App\Repositories\contracts;

interface ProductRepositoryInterface
{
    public function getById(int $id): ?object;

    public function create(array $data): object;

    public function update(array $data, int $id): bool;

    public function delete(int $id): bool;
}
