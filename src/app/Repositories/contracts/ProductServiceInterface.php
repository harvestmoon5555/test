<?php

namespace App\Repositories\contracts;

use App\Http\Resources\ProductResource;

interface ProductServiceInterface
{
    public function getOneProduct(int $id): ProductResource;

    public function createProduct(array $data): ProductResource;

    public function updateProduct(array $data, int $id);

    public function deleteProduct(int $id): void;

    public function buyProduct(int $id): bool;
}