<?php

namespace App\Repositories\contracts;

use Illuminate\Database\Eloquent\Collection;

interface ContentRepositoryInterface
{
    public function getById(int $id): ?object;

    public function getByContentable(int $contentableId, string $contentableType): Collection;

    public function create(array $data): object;

    public function update(array $data, int $id): bool;

    public function delete(int $id): bool;

    public function deleteByContentable(int $contentableId, string $contentableType): bool;
}
