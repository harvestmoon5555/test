<?php

namespace App\Repositories\contracts;

use Illuminate\Database\Eloquent\Collection;

interface ProductMetaRepositoryInterface
{
    public function getById(int $id): ?object;

    public function getByProductId(int $productId): Collection;

    public function create(array $data): object;

    public function update(array $data, int $id): bool;

    public function delete(int $id): bool;

    public function deleteByProductId(int $productId): bool;
}
