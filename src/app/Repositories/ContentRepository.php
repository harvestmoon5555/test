<?php

namespace App\Repositories;

use App\Models\Content;
use App\Repositories\contracts\ContentRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ContentRepository implements ContentRepositoryInterface
{
    public function __construct(protected Content $content)
    {
    }

    public function getById(int $id): ?object
    {
        return $this->content->find($id);
    }

    public function getByContentable(int $contentableId, string $contentableType): Collection
    {
        return $this->content
            ->where('contentable_type', $contentableType)
            ->where('contentable_id', $contentableId)
            ->get();
    }

    public function create(array $data): object
    {
        return $this->content->create($data);
    }

    public function update(array $data, int $id): bool
    {
       return $this->content->where('id', $id)->update($data);
    }

    public function delete(int $id): bool
    {
        return $this->content->where('id', $id)->delete();
    }

    public function deleteByContentable(int $contentableId, string $contentableType): bool
    {
        //Check if product has any contents
        $result = $this->content
            ->where('contentable_type', $contentableType)
            ->where('contentable_id', $contentableId)
            ->exists();
        if (!$result) {
            return true;
        }
        return $this->content
            ->where('contentable_type', $contentableType)
            ->where('contentable_id', $contentableId)
            ->delete();
    }
}
