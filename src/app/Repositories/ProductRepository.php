<?php

namespace App\Repositories;

use App\Repositories\contracts\ProductRepositoryInterface;

class ProductRepository implements ProductRepositoryInterface
{
    public function __construct(protected $product)
    {
    }

    public function getById(int $id): ?object
    {
        return $this->product->find($id);
    }

    public function create(array $data): object
    {
        return $this->product->create($data);
    }

    public function update(array $data, int $id): bool
    {
       return $this->product->where('id', $id)->update($data);
    }

    public function delete(int $id): bool
    {
        return $this->product->where('id',$id)->delete($id);
    }

}
