<?php

namespace App\Repositories;

use App\Repositories\contracts\ProductMetaRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ProductMetaRepository implements ProductMetaRepositoryInterface
{
    public function __construct(protected $productMeta)
    {
    }

    public function getById(int $id): ?object
    {
        return $this->productMeta->find($id);
    }

    public function create(array $data): object
    {
        return $this->productMeta->create($data);
    }

    public function update(array $data, int $id): bool
    {
      return  $this->productMeta->where('id', $id)->update($data);
    }

    public function delete(int $id): bool
    {
        return $this->productMeta->where('id', $id)->delete();
    }

    public function deleteByProductId(int $productId): bool
    {
        return $this->productMeta->where('product_id', $productId)->delete();
    }

    public function getByProductId(int $productId): Collection
    {
        return $this->productMeta->where('product_id', $productId)->get();
    }
}
