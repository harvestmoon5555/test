<?php

namespace App\Providers;

use App\Http\Services\ProductService;
use App\Models\Content;
use App\Models\Product;
use App\Models\ProductMeta;
use App\Repositories\ContentRepository;
use App\Repositories\contracts\ContentRepositoryInterface;
use App\Repositories\contracts\ProductMetaRepositoryInterface;
use App\Repositories\contracts\ProductRepositoryInterface;
use App\Repositories\contracts\ProductServiceInterface;
use App\Repositories\ProductMetaRepository;
use App\Repositories\ProductRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(ProductRepositoryInterface::class, function () {
            return new ProductRepository(new Product());
        });
        $this->app->bind(ProductMetaRepositoryInterface::class, function () {
            return new ProductMetaRepository(new ProductMeta());
        });
        $this->app->bind(ContentRepositoryInterface::class, function () {
            return new ContentRepository(new Content());
        });

        $this->app->bind(ProductServiceInterface::class, function () {
            return new ProductService(
                new ProductRepository(new Product()),
                new ProductMetaRepository(new ProductMeta()),
                new ContentRepository(new Content())
            );
        });
    }
}
